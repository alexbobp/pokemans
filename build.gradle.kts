plugins {
    // kotlin("jvm") version "1.3.21" // automatically applied
    // idea // automatically applied
//    id("voodoo") version "0.5.0-SNAPSHOT"
    id("voodoo") version "0.5.18-SNAPSHOT"
}

voodoo {
//    addTask(name = "build", parameters = listOf("build"))
//    addTask(name = "pack_sk", parameters = listOf("pack sk"))
//    addTask(name = "pack_mmc", parameters = listOf("pack mmc"))
////    addTask(name = "pack_mmc-static", parameters = listOf("pack mmc-static"))
////    addTask(name = "pack_mmc-fat", parameters = listOf("pack mmc-fat"))
//    addTask(name = "pack_server", parameters = listOf("pack server"))
////    addTask(name = "pack_curse", parameters = listOf("pack curse"))
//    addTask(name = "test_mmc", parameters = listOf("test mmc"))
//    addTask(name = "buildAndPackAll", parameters = listOf("build", "pack sk", "pack server", "pack mmc"))

    addTask("build") {
        build()
    }
    addTask(name = "pack_voodoo") {
        pack().voodoo()
    }
    addTask(name = "pack_mmc-voodoo") {
        pack().multimcVoodoo()
    }
    addTask(name = "pack_sk") {
        pack().sklauncher()
    }
    addTask(name = "pack_mmc-sk") {
        pack().multimcSk()
    }
    addTask(name = "pack_server") {
        pack().server()
    }
//    addTask(name = "pack_curse") {
//        pack().curse()
//    }
    addTask(name = "test_mmc") {
        test().multimc()
    }
    addTask(name = "buildAndPackAll") {
        build()
//        pack().sklauncher()
        pack().voodoo()
        pack().multimcVoodoo()
        pack().server()
//        pack().multimcSk()
//        pack().multimcSkFat()
//        pack().multimcFat()
//        pack().curse()
    }

    generateCurseforgeMods("Mod", "1.12", "1.12.1", "1.12.2", "1.14", "1.14.1", "1.14.2", "1.14.3", "1.14.4", "1.15.2")
    generateCurseforgeTexturepacks("TexturePack", "1.12", "1.12.1", "1.12.2", "1.14.4", "1.15.2")
    generateForge("Forge_12_2", "1.12.2")
    generateForge("Forge_14_4", "1.14.4")
    generateForge("Forge_15_2", "1.15.2")
}

//repositories {
//    maven(url = "http://maven.modmuss50.me/") {
//        name = "modmuss50"
//    }
//    maven(url = "https://kotlin.bintray.com/kotlinx") {
//        name = "kotlinx"
//    }
//    mavenCentral()
//    jcenter()
//}
//
//dependencies {
//    implementation(group = "moe.nikky.voodoo", name = "voodoo", version = "0.4.6+")
//    implementation(group = "moe.nikky.voodoo", name = "dsl", version = "0.4.6+")
//}
