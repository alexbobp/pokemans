import voodoo.dsl.builder.curse.CurseListBuilder
import voodoo.provider.CurseProvider

fun CurseListBuilder<CurseProvider>.pack() = group {
    validMcVersions = setOf("1.12.2", "1.12.1", "1.12")
    optional {
        selected = false
    }
}.list {
}
