# RosiePack 1.0

Property | Value
---|---
ID | `rosie`
Title | `RosiePack`
Pack Version | `1.0`
MC Version | `1.14.4`
Forge Version | `28.1.113`
Author | `Rosie`
No change in entries




