@file:GenerateForge(name = "Forge", mc = "1.12.2")
@file:GenerateMods(name = "Mod", mc = "1.12.2")
@file:GenerateMods(name = "Mod", mc = "1.12.1")
@file:GenerateMods(name = "Mod", mc = "1.12")

import voodoo.GenerateForge
import voodoo.GenerateMods
import voodoo.data.Side
import voodoo.data.UserFiles
import voodoo.provider.DirectProvider
import voodoo.provider.JenkinsProvider
import voodoo.provider.LocalProvider

mcVersion = "1.12.2"
title = "Pokemans"
authors = listOf("sahfy")
forge = Forge.mc1_12_2_latest

pack {
    //    baseUrl = "https://meowface.org/crafts/"
    multimc {
        skPackUrl = "https://meowface.org/crafts/pokemans.json"
    }
    skcraft {
        userFiles = UserFiles(
                include = listOf(
//                "options.txt",
//                "quark.cfg",
//                "foamfix.cfg"
                ),
                exclude = listOf("")
        )
    }
}

root(voodoo.provider.CurseProvider) {
    validMcVersions = setOf("1.12", "1.12.1", "1.12.2")
    list {
        withProvider(LocalProvider).list {
            +"Pixelmon" configure {
                fileSrc = "Pixelmon-1.12.2-7.0.3-universal.jar"
//                fileSrc = "PixelmonGenerations-1.12.2-2.5.1-universal.jar"
            }
        }

        // major tech
        +(Mod.industrialCraft)
        +(Mod.tinkersConstruct)
        +(Mod.enderIo)
        +(Mod.lootCapacitorTooltips)
        +(Mod.immersiveEngineering)
        +(Mod.thermalexpansion)
        +(Mod.thermallogistics)
        +(Mod.mekanism)
        +(Mod.refinedStorage)

        // OC
        +(Mod.opencomputers)
        +(Mod.opencomputersDriversForTinkersConstruct)
        +(Mod.openlights)
        +(Mod.openprinter)

        // minor tech
        +(Mod.chiselsBits)
        +(Mod.extraBitManipulation)
        +(Mod.openmodularturrets)
        +(Mod.storageDrawers)
        +(Mod.structuredCrafting)
        +(Mod.wearableBackpacks)
        +(Mod.darkUtilities)
        +(Mod.openblocks)
        +(Mod.betterBuildersWands)
        +(Mod.cookingForBlockheads)
        +(Mod.infraredstone)
        +(Mod.immersiveCables)
        +(Mod.grapplingHookMod)

        // magic
        +(Mod.astralSorcery)
        +(Mod.magicArsenal)
        +(Mod.morph)

        // pixelmon tech
        +(Mod.apricornTreeFarm) //TODO

        // perf
        +(Mod.betterfps)
        +(Mod.foamfixForMinecraft)
        +(Mod.fastworkbench)
        +(Mod.fastfurnace)
        +(Mod.sampler)

        // customization
        +(Mod.crafttweaker)
        +(Mod.customNpcs)
        +(Mod.lingeringLoot)
        +(Mod.modtweaker)
//        +(Mod.endertweaker)
        +(Mod.tails)

        // qol
        +(Mod.jei)
        +(Mod.jeiIntegration)
        +(Mod.quark)
        +(Mod.multiMine)
        +(Mod.journeymap)
        +(Mod.friendshipBracelet)
        +(Mod.mumblelink)

        // plants
        +(Mod.xlFoodMod)

        // gen
        +(Mod.roguelikeDungeons)
        +(Mod.theTwilightForest)

        // mechanics
        +(Mod.repose)
        +(Mod.chopDownUpdated)

        +(Mod.notenoughcodecs)
        +(Mod.charsetAudio)
        +(Mod.charsetBlockCarrying)
        +(Mod.charsetCrafting)
        +(Mod.charsetTools)
        +(Mod.charsetTablet)


        withProvider(DirectProvider).list {
            +"gameShark" configure {
                url = "https://pixelmonmod.com/mirror/sidemods/gameshark/5.2.1/gameshark-1.12.2-5.2.1-universal.jar"
                fileName = "gameshark-1.12.2-5.2.1-universal.jar"
            }
            +"tcg" configure {
                url = "https://pixelmonmod.com/mirror/sidemods/tcg/4.0.0/TCG-1.12.2-4.0.0-universal.jar"
                fileName = "TCG-1.12.2-4.0.0-universal.jar"
            }
        } //TODO


        +(Mod.antighost)
        +(Mod.flopper)

//        withProvider(JenkinsProvider) {
//            jenkinsUrl = "https://ci.elytradev.com"
//        }.list {
//            // Falkreon
//            +"thermionics" job "elytra/Thermionics/master"
//
//            // una
//            +"fruit-phone" job "elytra/FruitPhone/1.12.2"
//            +"probe-data-provider" job "elytra/ProbeDataProvider/1.12"
//        }


        // TODO: use elytra ci again if it comes back
        withProvider(LocalProvider).list {
            // Falkreon
            +"thermionics" configure {fileSrc = "Thermionics-MC1.12.2_ver1.1.4.jar"}

            // una
            +"fruit-phone" configure {fileSrc = "FruitPhone-1.12.2-2.86.jar"}
            +"probe-data-provider" configure {fileSrc = "ProbeDataProviderAPI-MC1.12_ver1.1.1.jar"}
        }


        group {
            side = Side.CLIENT
        }.list {
            +(Mod.reauth)
//            +(Mod.keyboardWizard) TODO
//            +(Mod.controlling) TODO
            +(Mod.toastControl)
            +(Mod.vise)
            +(Mod.mage)
        }

        group {
            side = Side.SERVER
        }.list {
            +(Mod.btfuContinuousRsyncIncrementalBackup)
            +(Mod.nuclearOption)
            +(Mod.swingthroughgrass)
            +(Mod.colorchat)
            +(Mod.mtqfix)
            withProvider(JenkinsProvider) {
                jenkinsUrl = "https://ci.elytradev.com"
            }.list {
                //                +"matterlink" job "elytra/MatterLink/master"   TODO
            }
            withProvider(DirectProvider).list {
                +"pixelAnnouncer" configure {
                    url = "https://pixelmonmod.com/mirror/sidemods/PixelAnnouncer/1.1.0/PixelAnnouncer-1.12.2-1.1.0.jar"
                    fileName = "PixelAnnouncer-1.12.2-1.1.0.jar"
                }
            } //TODO
        }
    }
}
