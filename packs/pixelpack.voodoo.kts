mcVersion = "1.12.2"
title = "pixelpack"
authors = listOf("pixel", "lizzie")
modloader {
    forge(Forge_12_2.mc1_12_2.forge_14_23_5_2847)
}

pack {
    //    baseUrl = "https://meowface.org/crafts/"
    multimc {
        skPackUrl = "https://meowface.org/crafts/pixelpack.json"
    }
    skcraft {
        userFiles = UserFiles(
                include = listOf(
//                "options.txt",
//                "quark.cfg",
//                "foamfix.cfg"
                ),
                exclude = listOf("")
        )
    }
}

root<Curse> {
    validMcVersions = setOf("1.12", "1.12.1", "1.12.2")
    it.list {


//        +(Mod.treecapitatorPort)
//        +(Mod.appliedEnergistics2)
//        +(Mod.architecturecraftElytradev)
//        +(Mod.betterStorageToo)
//        +(Mod.)
//        +(Mod.)
//        +(Mod.)
//        +(Mod.)
//        +(Mod.)
//        +(Mod.)
//        +(Mod.)
//        +(Mod.)
//        +(Mod.)
//
//        // OC
//        +(Mod.opencomputers)
//        +(Mod.opencomputersDriversForTinkersConstruct)
//        +(Mod.openlights)
//        +(Mod.openprinter)
//
//        // minor tech
//        +(Mod.chiselsBits)
//        +(Mod.extraBitManipulation)
//        +(Mod.openmodularturrets)
//        +(Mod.storageDrawers)
//        +(Mod.structuredCrafting)
//        +(Mod.wearableBackpacks)
//        +(Mod.darkUtilities)
//        +(Mod.openblocks)
//        +(Mod.betterBuildersWands)
//        +(Mod.cookingForBlockheads)
//        +(Mod.infraredstone)
//        +(Mod.immersiveCables)
//        +(Mod.grapplingHookMod)
//
//        // perf
//        +(Mod.betterfps)
//        +(Mod.foamfixForMinecraft)
//        +(Mod.fastworkbench)
//        +(Mod.fastfurnace)
//        +(Mod.sampler)
//
//        // customization
//        +(Mod.crafttweaker)
//        +(Mod.customNpcs)
//        +(Mod.lingeringLoot)
//        +(Mod.modtweaker)
////        +(Mod.endertweaker)
//        +(Mod.tails)
//
//        // qol
//        +(Mod.jei)
//        +(Mod.jeiIntegration)
//        +(Mod.quark)
//        +(Mod.multiMine)
//        +(Mod.journeymap)
//        +(Mod.friendshipBracelet)
//        +(Mod.mumblelink)
//
//        // plants
//        +(Mod.xlFoodMod)
//
//        // gen
//        +(Mod.roguelikeDungeons)
//        +(Mod.theTwilightForest)
//
//        // mechanics
//        +(Mod.repose)
//        +(Mod.chopDownUpdated)
//
//        +(Mod.notenoughcodecs)
//        +(Mod.charsetAudio)
//        +(Mod.charsetBlockCarrying)
//        +(Mod.charsetCrafting)
//        +(Mod.charsetTools)
//        +(Mod.charsetTablet)


        withProvider(DirectProvider).list {
//            +"modname" configure {
//                url = "https://pixelmonmod.com/mirror/sidemods/gameshark/5.2.1/gameshark-1.12.2-5.2.1-universal.jar"
//                fileName = "gameshark-1.12.2-5.2.1-universal.jar"
//            }
        } //TODO


//        +(Mod.antighost)
//        +(Mod.flopper)

        withProvider(JenkinsProvider) {
            jenkinsUrl = "https://ci.elytradev.com"
        }.list {
            // una
            +"fruit-phone" job "elytra/FruitPhone/1.12.2"
            +"probe-data-provider" job "elytra/ProbeDataProvider/1.12"
        }


        // TODO: use elytra ci again if it comes back
        withProvider(LocalProvider).list {
//            // Falkreon
//            +"thermionics" configure {fileSrc = "Thermionics-MC1.12.2_ver1.1.4.jar"}
//
//            // una
//            +"fruit-phone" configure {fileSrc = "FruitPhone-1.12.2-2.86.jar"}
//            +"probe-data-provider" configure {fileSrc = "ProbeDataProviderAPI-MC1.12_ver1.1.1.jar"}
        }


        group {
            side = Side.CLIENT
        }.list {
//            +(Mod.reauth)
////            +(Mod.keyboardWizard) TODO
////            +(Mod.controlling) TODO
//            +(Mod.toastControl)
//            +(Mod.vise)
//            +(Mod.mage)
        }

        group {
            side = Side.SERVER
        }.list {
//            +(Mod.btfuContinuousRsyncIncrementalBackup)
//            +(Mod.nuclearOption)
//            +(Mod.swingthroughgrass)
//            +(Mod.colorchat)
//            +(Mod.mtqfix)
            withProvider(JenkinsProvider) {
                jenkinsUrl = "https://ci.elytradev.com"
            }.list {
                //                +"matterlink" job "elytra/MatterLink/master"   TODO
            }
            withProvider(DirectProvider).list {
//                +"pixelAnnouncer" configure {
//                    url = "https://pixelmonmod.com/mirror/sidemods/PixelAnnouncer/1.1.0/PixelAnnouncer-1.12.2-1.1.0.jar"
//                    fileName = "PixelAnnouncer-1.12.2-1.1.0.jar"
//                }
            } //TODO
        }
    }
}


/*
-rw-r--r--  1 pixel pixel 21575745 Sep  1  2018  1.7.10_AM2-1.4.0.009.jar

-rw-r--r--  1 pixel pixel    14859 Sep  1  2018  AnimationAPI-1.7.10-1.2.4.jar

-rw-r--r--  1 pixel pixel   160402 Sep  1  2018  AsieLib-1.7.10-0.4.8.jar
-rw-r--r--  1 pixel pixel   710424 Sep  1  2018  BetterStorage-1.7.10-0.13.1.128.jar
-rw-r--r--  1 pixel pixel  5452316 Sep  1  2018 'BiblioCraft[v1.11.7][MC1.7.10].jar'
-rw-r--r--  1 pixel pixel   771107 Sep  1  2018  BigReactors-0.4.3A.jar
-rw-r--r--  1 pixel pixel  4562769 Sep  1  2018  binnie-mods-1.7.10-2.0.22.7.jar
-rw-r--r--  1 pixel pixel  5755198 Sep  1  2018  BiomesOPlenty-1.7.10-2.1.0.2308-universal.jar
-rw-r--r--  1 pixel pixel  9198412 Sep  1  2018  BloodMagic-1.7.10-1.3.3-17.jar
-rw-r--r--  1 pixel pixel  2705733 Sep  1  2018  buildcraft-7.1.23.jar
-rw-r--r--  1 pixel pixel   259292 Sep  1  2018  buildcraft-compat-7.1.7.jar
-rw-r--r--  1 pixel pixel   513792 Sep  1  2018 "Carpenter's Blocks v3.3.8.1 - MC 1.7.10.jar"
-rw-r--r--  1 pixel pixel   106701 Sep  1  2018  ChickenChunks-1.7.10-1.3.4.19-universal.jar
-rw-r--r--  1 pixel pixel  6378920 Sep  1  2018  Chisel-2.9.5.11.jar
-rw-r--r--  1 pixel pixel   145782 Sep  1  2018  CodeChickenCore-1.7.10-1.0.7.47-universal.jar
-rw-r--r--  1 pixel pixel  1230511 Sep  1  2018 'CoFHCore-[1.7.10]3.1.4-329.jar'
-rw-r--r--  1 pixel pixel   325985 Sep  1  2018  compactmachines-1.7.10-1.21.jar
-rw-r--r--  1 pixel pixel  1397415 Sep  1  2018  ComputerCraft1.75.jar
-rw-r--r--  1 pixel pixel  1171598 Sep  1  2018  Computronics-1.7.10-1.6.5.jar
-rw-r--r--  1 pixel pixel   145129 Sep  1  2018  Cosmetic-Wings-1.7.10-0.4.2.17.jar
-rw-r--r--  1 pixel pixel    64462 Sep  1  2018  D3Core-1.7.10-1.1.2.41.jar
-rw-r--r--  1 pixel pixel   453341 Sep  1  2018  DragonMounts-r41-1.7.10.jar
-rw-r--r--  1 pixel pixel   467824 Sep  1  2018  EnderCore-1.7.10-0.2.0.39_beta.jar
-rw-r--r--  1 pixel pixel  4756103 Sep  1  2018  EnderIO-1.7.10-2.3.0.429_beta.jar
-rw-r--r--  1 pixel pixel   145460 Sep  1  2018  EnderStorage-1.7.10-1.4.7.38-universal.jar
-rw-r--r--  1 pixel pixel  2149827 Sep  1  2018  extrautilities-1.2.12.jar
-rw-r--r--  1 pixel pixel  3322286 Sep  1  2018  forestry_1.7.10-4.2.16.64.jar
-rw-r--r--  1 pixel pixel  6229361 Sep  1  2018  FSP-1.7.10-0.29.3.jar
-rw-r--r--  1 pixel pixel   283132 Sep  1  2018  Hats-4.0.1.jar
-rw-r--r--  1 pixel pixel   213084 Sep  1  2018  iChunUtil-4.2.3.jar
-rw-r--r--  1 pixel pixel  3610468 Sep  1  2018  ImmersiveEngineering-0.7.7.jar
-rw-r--r--  1 pixel pixel    25189 Sep  1  2018  InfiniBows-1.7.10-universal-coremod-1.3.0.20.jar
-rw-r--r--  1 pixel pixel   224461 Sep  1  2018  InventoryTweaks-1.59-dev-152.jar
-rw-r--r--  1 pixel pixel   158104 Sep  1  2018  ironchest-1.7.10-6.0.62.742-universal.jar
-rw-r--r--  1 pixel pixel   445166 Sep  1  2018  Jabba-1.2.2_1.7.10.jar
-rw-r--r--  1 pixel pixel  1731255 Sep  1  2018  journeymap-1.7.10-5.1.4p2-unlimited.jar
-rw-r--r--  1 pixel pixel  3250639 Sep  1  2018  logisticspipes-0.9.3.132.jar
-rw-r--r--  1 pixel pixel   202458 Sep  1  2018  Mantle-1.7.10-0.3.2b.jar
-rw-r--r--  1 pixel pixel   187881 Sep  1  2018  mcjtylib-1.8.1.jar
-rw-r--r--  1 pixel pixel  1756014 Sep  1  2018 'MineFactoryReloaded-[1.7.10]2.8.1-174.jar'
-rw-r--r--  1 pixel pixel    18767 Sep  1  2018  Morpheus-1.7.10-1.6.21.jar
-rw-r--r--  1 pixel pixel   818633 Sep  1  2018  NotEnoughCodecs-1.7.10-0.4.jar
-rw-r--r--  1 pixel pixel   513136 Sep  1  2018  NotEnoughItems-1.7.10-1.0.5.120-universal.jar
-rw-r--r--  1 pixel pixel    29473 Sep  1  2018  NotEnoughKeys-1.7.10-3.0.0b45-dev-universal.jar
-rw-r--r--  1 pixel pixel  2799187 Sep  1  2018  OpenBlocks-1.7.10-1.6.jar
-rw-r--r--  1 pixel pixel 14901904 Sep  1  2018  OpenComputers-MC1.7.10-1.7.2.1166-universal.jar
-rw-r--r--  1 pixel pixel   194047 Sep  1  2018  OpenEye-1.7.10-0.8.jar
-rw-r--r--  1 pixel pixel  2259572 Sep  1  2018  OpenModsLib-1.7.10-0.10.jar
-rw-r--r--  1 pixel pixel  2482753 Sep  1  2018 "Pam's+HarvestCraft+1.7.10Lb.jar"
-rw-r--r--  1 pixel pixel  1432027 Sep  1  2018  ProjectRed-1.7.10-4.7.0pre12.95-Base.jar
-rw-r--r--  1 pixel pixel    84757 Sep  1  2018  ProjectRed-1.7.10-4.7.0pre12.95-Compat.jar
-rw-r--r--  1 pixel pixel  1654657 Sep  1  2018  ProjectRed-1.7.10-4.7.0pre12.95-Fabrication.jar
-rw-r--r--  1 pixel pixel  3913048 Sep  1  2018  ProjectRed-1.7.10-4.7.0pre12.95-Integration.jar
-rw-r--r--  1 pixel pixel   633621 Sep  1  2018  ProjectRed-1.7.10-4.7.0pre12.95-Lighting.jar
-rw-r--r--  1 pixel pixel  2118480 Sep  1  2018  ProjectRed-1.7.10-4.7.0pre12.95-Mechanical.jar
-rw-r--r--  1 pixel pixel   478879 Sep  1  2018  ProjectRed-1.7.10-4.7.0pre12.95-World.jar
-rw-r--r--  1 pixel pixel   318347 Sep  1  2018 'RedstoneArsenal-[1.7.10]1.1.2-92.jar'
-rw-r--r--  1 pixel pixel  3447051 Sep  8  2018  rftools-4.13.jar
-rw-r--r--  1 pixel pixel   608053 Sep  1  2018  SimplyJetpacks-MC1.7.10-1.5.3.jar
-rw-r--r--  1 pixel pixel   174707 Sep  1  2018  SolarFlux-1.7.10-0.8b.jar
-rw-r--r--  1 pixel pixel 12565807 Sep  1  2018  Thaumcraft-1.7.10-4.2.3.5.jar
-rw-r--r--  1 pixel pixel        0 Sep  1  2018  ThaumicExpansion-1.7.10-0.0.1.jar
-rw-r--r--  1 pixel pixel  1982603 Sep  1  2018  ThaumicExploration-1.7.10-1.1-53.jar
-rw-r--r--  1 pixel pixel  2141043 Sep  1  2018  ThaumicTinkerer-2.5-1.7.10-164.jar
-rw-r--r--  1 pixel pixel   632485 Sep  1  2018 'ThermalDynamics-[1.7.10]1.2.1-172.jar'
-rw-r--r--  1 pixel pixel  2614932 Sep  1  2018 'ThermalExpansion-[1.7.10]4.1.5-248.jar'
-rw-r--r--  1 pixel pixel  2316321 Sep  1  2018 'ThermalFoundation-[1.7.10]1.2.6-118.jar'
-rw-r--r--  1 pixel pixel     5105 Sep  1  2018  TorchTools-1.7.10-1.2.0.27.jar
-rw-r--r--  1 pixel pixel   161165 Sep  1  2018  Translocator-1.7.10-1.1.2.16-universal.jar
-rw-r--r--  1 pixel pixel   544821 Sep  1  2018  Waila-1.5.10_1.7.10.jar
-rw-r--r--  1 pixel pixel    28800 Sep  1  2018  WailaHarvestability-mc1.7.10-1.1.6.jar
-rw-r--r--  1 pixel pixel   199805 Sep  1  2018  WirelessCommunication-1.7.10-1.0.jar
-rw-r--r--  1 pixel pixel  7009956 Sep  1  2018  witchery-1.7.10-0.24.1.jar
-rw-r--r--  1 pixel pixel   352855 Sep  1  2018  WR-CBE-1.7.10-1.4.1.11-universal.jar
 */