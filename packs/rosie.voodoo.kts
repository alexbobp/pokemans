@file:GenerateForge(name = "Forge", mc = "1.14.4")
@file:GenerateMods(name = "Mod", mc = "1.14.4")
@file:GenerateMods(name = "Mod", mc = "1.14.3")
@file:GenerateMods(name = "Mod", mc = "1.14.2")

import voodoo.GenerateForge
import voodoo.GenerateMods
import voodoo.data.Side
import voodoo.data.UserFiles
import voodoo.provider.DirectProvider
import voodoo.provider.JenkinsProvider
import voodoo.provider.LocalProvider

mcVersion = "1.14.4"
title = "RosiePack"
authors = listOf("Rosie")
//forge = Forge.mc1_12_2.forge_14_23_5_2835
//forge = "1.12.2-14.23.5.2838" // Forge.mc1_12_2_latest TODO charset
forge = Forge.mc1_14_4_latest


pack {

}