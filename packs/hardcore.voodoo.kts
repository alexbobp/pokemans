mcVersion = "1.12.2"
title = "Hardcore"
authors = listOf("lizzie")
modloader {
    forge(Forge_12_2.mc1_12_2.forge_14_23_5_2847)
}

pack {
    multimc {
        selfupdateUrl = "https://meowface.org/crafts/hardcore.json"
        instanceCfg = listOf(
                "OverrideMemory" to "true",
                "MaxMemAlloc" to "4000",
                "MinMemAlloc" to "4000"
        )
    }
    skcraft {
        userFiles = UserFiles(
                include = listOf(
                        "options.txt",
                        "quark.cfg",
                        "mapwriter.cfg"
//                "foamfix.cfg"
                ),
                exclude = listOf("")
        )
    }
}

root<Curse> {
    validMcVersions = setOf("1.12", "1.12.1", "1.12.2")
    it.list {
        withTypeClass(Local::class).list {
            +"Liberties" {
                fileSrc = "Liberties-1.jar"
            }
        }

        +(Mod.hwyla)

        // major tech
//        +(Mod.industrialCraft)
        +(Mod.ic2Classic) // TODO remove quantumsuit and electric jetpack
        //+(Mod.ic2Rpg) // TODO if using DivineRPG or Advent of Ascension
//        withTypeClass(Local::class).list {
//            +"ic2" {
//                fileSrc = "industrialcraft-2-2.8.195-ex112.jar" // 196 is broken
//            }
//        }

        +(Mod.roots)

        +(Mod.railcraft)
        +(Mod.buildcraft)
        +(Mod.buildcraftFuelsForIc2)
//        +(Mod.buildcraftCompat)

        withTypeClass(Local::class).list {
            +"buildcraft-core" {
                fileSrc = "nothing"
            }
        }

        +(Mod.appliedEnergistics2)

        +(Mod.roboticparts) // check for cybereye exploit, check for cost counting issues

        +(Mod.stackup)
        +(Mod.stackable)

        +(Mod.charm)

        +(Mod.charsetImmersion)
        +(Mod.charsetBlockCarrying)
        +(Mod.charsetCrafting)
        +(Mod.charsetStorageChests)
        +(Mod.charsetStorageBarrels)
        +(Mod.charsetStorageTanks)
        +(Mod.charsetTools)
        withTypeClass(Local::class).list {
            +"charsetOptics" {
                fileSrc = "Charset-Optics-0.5.6.3.jar"
            }
        }

        +Mod.cosmeticArmorReworked
        +Mod.loadout // TODO stuff loadout

        // minor tech
        +(Mod.structuredCrafting)
        +(Mod.wearableBackpacks) // TODO big inventory but replace armor


        +(Mod.darkUtilities) // TODO
//        withTypeClass(Local::class).list {
//            +"darkUtilities" configure {
//                fileSrc =
//                        "DarkUtils-1.12.2-1.8.0.jar"
//            }
//        }
//        +Mod.bookshelf // TODO lib for darkutil

        +(Mod.openblocks) // TODO ban most shit
        +(Mod.infraredstone) // todo test

        // perf
        +(Mod.betterfps)
        +(Mod.foamfixOptimizationMod)
        +(Mod.fastworkbench)
        +(Mod.sampler)
        +(Mod.caliper) // TODO lern
        +(Mod.phosphorForge) // TODO see if works

        // customization
        +(Mod.crafttweaker)
        +(Mod.lingeringLoot)
        +(Mod.modtweaker)
        +(Mod.tails)
        +(Mod.cravings) // TODO conf from pokemans
        +Mod.randompatches
        +Mod.randomtweaks

        // qol
        +(Mod.jei)
        +(Mod.jeiIntegration)
//        +(Mod.quark) // TODO see if I actually want this shite

        +(Mod.swingthroughgrass)
        +Mod.mapwriter2
        +Mod.mxtune
        +Mod.naturesCompass
        +Mod.appleskin

        +Mod.theAether

        // gen
        +(Mod.roguelikeDungeons)

        // mechanics
        +(Mod.chopDownUpdated) // TODO: apricorn trees
        +(Mod.repose)


        +(Mod.antighost)

//        withTypeClass(Jenkins::class) {
//            jenkinsUrl = "https://ci.elytradev.com"
//
//        }.list {
//            // Falkreon
//            +"thermionics" job "elytra/Thermionics/master"
//        }
        withTypeClass(Local::class).list {
            // Falkreon
            +"thermionics" configure {fileSrc = "Thermionics-MC1.12.2_ver1.1.4.jar"}

            // una
//            +"probe-data-provider" configure {fileSrc = "ProbeDataProviderAPI-MC1.12_ver1.1.1.jar"}
        }


        group {
            side = Side.CLIENT
        }.list {
            +(Mod.fastcraftingmod) // TODO test
            +(Mod.reauth)
            +(Mod.controlling)
            +(Mod.toastControl)
            +(Mod.vise)
            +(Mod.mumblelink)
            +(Mod.worldeditcuiForgeEdition2)

            +Mod.itemScroller // TODO
        }

        group {
            side = Side.SERVER
        }.list {
            +(Mod.btfuContinuousRsyncIncrementalBackup)
//            +(Mod.nuclearOption) // TODO only introduce this if the server has perf leak issues
            +(Mod.colorchat)
            +(Mod.mtqfix)
            +(Mod.worldedit) // TODO
            +(Mod.customStarterGear)
            +(Mod.morpheus) // TODO sleepy thing
//            withTypeClass(Jenkins::class) {
//                jenkinsUrl = "https://ci.elytradev.com"
//            }.list {
////                +"matterlink" job "elytra/MatterLink/master"   TODO
//                +"DimensionalGatekeeper" job "elytra/DimensionalGatekeeper/master"
//            }
//            withTypeClass(Local::class).list {
//                +"DimensionalGatekeeper" configure {fileSrc = "DimensionalGatekeeper-1.12.2-1.jar"}
//            }
        }
    }
}
