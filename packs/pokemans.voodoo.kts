mcVersion = "1.12.2"
version = "1.0.0"
title = "Pokemans"
authors = listOf("lizzie")
//modloader {
//    forge(Forge_12_2.mc1_12_2.forge_14_23_5_2847)
//}
modloader { forge(Forge_12_2.mc1_12_2_latest) }

pack {
//    baseUrl = "https://meowface.org/crafts/"
    multimc {
        selfupdateUrl = "https://meowface.org/crafts/pokemans.json"
        instanceCfg = listOf(
                "OverrideMemory" to "true",
                "MaxMemAlloc" to "4000",
                "MinMemAlloc" to "4000"
        )
    }
    voodoo {
        userFiles = FnPatternList(
            include = listOf(
                "options.txt",
                "quark.cfg",
                "mapwriter.cfg"
//                "foamfix.cfg"
            ),
            exclude = listOf("")
        )
    }
}

root<Curse> {
    validMcVersions = setOf("1.12", "1.12.1", "1.12.2")
    it.list {
        withTypeClass(Local::class).list {
            +"Pixelmon" {
//                fileSrc = "Pixelmon-1.12.2-7.0.7-universal.jar"
//                fileSrc = "Pixelmon-1.12.2-7.2.2-universal.jar"
                fileSrc = "Pixelmon-1.12.2-8.0.2-universal.jar"
//                fileSrc = "PixelmonGenerations-1.12.2-2.5.1-universal.jar"
            }
            +"Liberties" {
                fileSrc = "Liberties-1.jar"
            }
        }

        // major tech
//        +(Mod.industrialCraft)
        withTypeClass(Local::class).list {
            +"ic2" {
                fileSrc = "industrialcraft-2-2.8.195-ex112.jar" // 196 is broken
            }
        }

//        +(Mod.tinkersConstruct)
        withTypeClass(Local::class).list {
            +"tinkers-construct" {
//                fileSrc = "TConstruct-1.12.2-2.12.0.DEV.d74d5d1.jar" // Slimesling Other
                fileSrc = "TConstruct-1.12.2-2.13.0.DEV.d2c3ff13f.jar" // all new ids? :o
            }
        }
        +(Mod.mantle)

        +(Mod.enderIoConduits)
            +(Mod.enderIoRsConduits)
            +(Mod.enderIoTic)
        +(Mod.enderIoMachines)
            +(Mod.lootCapacitorTooltips)
        +(Mod.immersiveEngineering)
        +(Mod.thermalExpansion) // TODO hammer recipe conflict with pixelmon
        +(Mod.thermallogistics)
        +Mod.redstoneFlux
        +(Mod.mekanism)
            +(Mod.mekanismGenerators)
            +(Mod.mekanismTools)
        +(Mod.gasConduits)
//        withTypeClass(Local::class).list {
//            +"mekanism" {
//                fileSrc = "Mekanism-1.12.2-9.6.9.367.jar"
//            }
//            +"mekanism-generators" {
//                fileSrc = "MekanismGenerators-1.12.2-9.6.9.367.jar"
//            }
//            +"mekanism-tools" {
//                fileSrc = "MekanismTools-1.12.2-9.6.9.367.jar"
//            }
//            +"gas-conduits" { // TODO ?
//                fileSrc = "GasConduits-1.12.2-1.1.0.jar"
//            }
//        }
        +(Mod.refinedStorage)
        +(Mod.enderStorage18)

        // OC
        +(Mod.opencomputers)
        +(Mod.opencomputersDriversForTinkersConstruct) //TODO whines about tinker's version?
        +(Mod.openprinter)

        // minor tech
        +(Mod.chiselsBits)
        +(Mod.extraBitManipulation)
        +(Mod.openmodularturrets)
        +(Mod.storageDrawers)
        +(Mod.structuredCrafting)
        +(Mod.wearableBackpacks)


//        +(Mod.darkUtilities) // TODO
        withTypeClass(Local::class).list {
            +"darkUtilities" configure {
                fileSrc =
                        "DarkUtils-1.12.2-1.8.0.jar"
            }
        }
        +Mod.bookshelf // TODO lib for darkutil

        +(Mod.openblocks)
        +(Mod.betterBuildersWands)
        +(Mod.cookingForBlockheads)
        +(Mod.infraredstone)
        +(Mod.immersiveCables)
        +(Mod.grapplingHookMod)

        // magic
        +(Mod.magicArsenal)
//        +(Mod.morph)
        +(Mod.mystcraft)
        +(Mod.mystcraftInfo)

        // pixelmon tech
        +(Mod.apricornTreeFarm)

        // perf
        +(Mod.betterfps)
        +(Mod.foamfixOptimizationMod)
        +(Mod.fastworkbench)
        //+(Mod.fastfurnace) // TODO diagnose bug
        +(Mod.sampler)
        +(Mod.caliper) // TODO
//        +Mod.phosphor // TODO
//        withTypeClass(Local::class).list {
//            +"phosphor" configure {
//                fileSrc =
////                        "phosphor-1.12.2-0.1.6+build31-universal.jar"
//                        "phosphor-1.12.2-0.1.6-SNAPSHOT-universal.jar"
//            }
//        }

        // customization
        +(Mod.crafttweaker)
        +(Mod.lingeringLoot)
        +(Mod.modtweaker)
//        +(Mod.endertweaker)
        +(Mod.tails)
        +(Mod.cravings)
        +Mod.randompatchesIntegration

        // qol
        +(Mod.jei)
        +(Mod.jeiIntegration)
        +(Mod.quark)
//        +(Mod.multiMine) // TODO see if lag
        +(Mod.friendshipBracelet)
        +(Mod.swingthroughgrass)
        +Mod.autoFeederHelmet
        +Mod.mapwriter2
        +Mod.mxtune
        +Mod.naturesCompass
        +Mod.appleskin


        // plants
//        +(Mod.xlFoodMod) // TODO check if gone is ok
        +(Mod.rustic) // TODO check if ok

        // gen
        +(Mod.roguelikeDungeons)
        +(Mod.theTwilightForest)

        // mechanics
//        +(Mod.nocubes)
//        +(Mod.repose) // TODO fucks up default gen stuffs...
        +(Mod.chopDownUpdated) // TODO: apricorn trees

        +(Mod.notenoughcodecs)
        +(Mod.charsetAudio)
        +(Mod.charsetBlockCarrying)
        +(Mod.charsetCrafting)
        +(Mod.charsetTools)
        +(Mod.charsetTablet)

//        +(Mod.biomeBundle)

        withTypeClass(Direct::class).list {
            +"gameShark" configure {
                url = "https://pixelmonmod.com/mirror/sidemods/gameshark/6.0.4/Gameshark-1.12.2-6.0.4-universal.jar"
                fileName = "Gameshark-1.12.2-6.0.4-universal.jar"
            }
            +"tcg" configure {
                url = "https://pixelmonmod.com/mirror/sidemods/tcg/4.0.1/TCG-1.12.2-4.0.1-universal.jar"
                fileName = "TCG-1.12.2-4.0.1-universal.jar"
            }
        }


        +(Mod.antighost)
        +(Mod.flopper)

//        withTypeClass(Jenkins::class) {
//            jenkinsUrl = "https://ci.elytradev.com"
//
//        }.list {
//            // Falkreon
//            +"thermionics" job "elytra/Thermionics/master"
//
//            // una
//            +"fruit-phone" job "elytra/FruitPhone/1.12.2"
//            +"probe-data-provider" job "elytra/ProbeDataProvider/1.12"
//        }
        withTypeClass(Local::class).list {
            // Falkreon
            +"thermionics" configure {fileSrc = "Thermionics-MC1.12.2_ver1.1.4.jar"}

            // una
            +"fruit-phone" configure {fileSrc = "FruitPhone-1.12.2-2.86.jar"}
            +"probe-data-provider" configure {fileSrc = "ProbeDataProviderAPI-MC1.12_ver1.1.1.jar"}
        }


        group {
            side = Side.CLIENT
        }.list {
            +(Mod.reauth)
//            +(Mod.keyboardWizard) // causes issue binding modifier keys
            +(Mod.controlling)
            +(Mod.toastControl)
            +(Mod.vise)
            +(Mod.mage)
            +(Mod.mumblelink)
            +(Mod.worldeditcuiForgeEdition2)

            +Mod.itemScroller // TODO
        }

        group {
            side = Side.SERVER
        }.list {
            +(Mod.btfuContinuousRsyncIncrementalBackup)
            +(Mod.nuclearOption)
            +(Mod.colorchat)
            +(Mod.mtqfix)
            +(Mod.worldedit) // TODO
            +(Mod.customStarterGear)
            +(Mod.morpheus) // TODO sleepy thing
//            withTypeClass(Jenkins::class) {
//                jenkinsUrl = "https://ci.elytradev.com"
//            }.list {
////                +"matterlink" job "elytra/MatterLink/master"   TODO
//                +"DimensionalGatekeeper" job "elytra/DimensionalGatekeeper/master"
//            }
            withTypeClass(Local::class).list {
                +"DimensionalGatekeeper" configure {fileSrc = "DimensionalGatekeeper-1.12.2-1.jar"}
            }
            withTypeClass(Direct::class).list {
                +"pixelAnnouncer" configure {
                    url = "https://pixelmonmod.com/mirror/sidemods/PixelAnnouncer/1.1.0/PixelAnnouncer-1.12.2-1.1.0.jar"
                    fileName = "PixelAnnouncer-1.12.2-1.1.0.jar"
                }
            }
        }
    }
}
